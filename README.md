# 欢迎使用 PAP 快速开发框架

------

本项目整合了部分 PAP 业务线下部分基础组件与业务组件，更加详细的组件模块介绍可以进入到各个子模块中查看ReadME。


### <a href="http://198.35.44.102:8080/" target="_blank">访问 DEMO</a>

```
自定义starter
# pap-logback-operdb-spring-boot-starter 
 Interceptor 注解，异步持久化到日志数据库，解决操作日志问题
 
# pap-sequence-starter 
 流水号生成器，形如 年月日自增流水号(可增加前缀与后缀)
 
 # pap-bean-spring-boot-starter
 自定义业务Bean，形如 IDWorker序号生成器/当前登录用户处理Bean
```

```
LogBack日志插件
# pap-logback-ext
 在持久化数据库的过程中，对logback-core扩展，使其可以最多保存32个参数。
 对时间格式进行可视化处理。
 
# pap-logback-oper
 多数据源下日志查询(为防止日志持久化到多个不同数据库)，这里需要动态切换数据源。
```

```
基础组件
# pap-calculate
 自定义数学公式计算器，以实现绝大部分Excel中的公式。
 
# pap-code-generator
 代码生成器，支持Mybatis自定义扩展插件、FreeMarker 自定义模板。
```

```
基础组件
# pap-upload
 同一文件处理，支持 ElementUI 。
 
# pap-obj
 基础POJO类、含部分工具类
 
# pap-base
 基础Base通用方法与额外工具类
 
# pap-activitiy
 工作流处理逻辑，以实现 发布模型，发布任务，领取任务，审核任务，任务流程图，与RBAC用户体系打通。
 
# pap-rabbitmq
 消息中间件工具类，其中包含 分布式事务过程中针对消息中间件的支持，增加 事务协调者(守护进程) 的概念，保证分布式事务的最终一致性。
 
# pap-spring-boot-spi-demo
 Saas 平台在设计的过程中，会出现不同租户的业务需求不一致的情况，在这种情况下，则可以考虑使用 SPI 来支持定制化的服务。
  
# pap-spring-boot-admin
 管理和监控Spring Boot 应用程序的开源软件，增加Eureka 的支持，并且处理项目上下文的情况。
```

```
业务组件
# pap-datas
 省市区、数据字典功能 。
 
# pap-rbac
 RBAC用户权限模型，其中因为前后端分离，对RBAC进行部分修改，采用 权限码 进行前后端的权限约定，对各类资源都采用如上的 权限编码 概念进行权限控制。
 
# pap-msg
 系统消息、站内信。
 
# pap-customer
 客户中心。
 
# pap-item
 电商环节，商品中心(其中包含SKU 的处理逻辑，详见子模块的ReadMe 文件)。
  
# pap-product
 产品中心，对自定义公式的汇总，可以通过自定义复杂的公式，进行四则运算，可以满足形如 薪酬计算 等需要公式介入的功能。
 
# d2-admin-start-kit
  前端项目，使用：  https://github.com/d2-projects/d2-admin 
```

![image](http://198.35.44.102:8080/demo.gif)

前端依赖
<a href="https://github.com/d2-projects/d2-admin" target="_blank"><img src="https://raw.githubusercontent.com/FairyEver/d2-admin/master/doc/image/d2-admin@2x.png" width="200"></a>